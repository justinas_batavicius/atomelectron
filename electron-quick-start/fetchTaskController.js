angular.module('httpExample2', [])
    .controller('FetchTaskController', ['$scope', '$http', '$templateCache',
        function ($scope, $http, $templateCache) {
            $scope.url = 'http://localhost/public/api/task';
            $scope.fetch = function () {
                $scope.code = null;
                $scope.response = null;
                $http({method: "GET", url: $scope.url, cache: $templateCache}).
                then(function (response) {//if okay
                    $scope.status = response.status;
                    $scope.data = response.data;
                    console.log($scope.data);
                }, function (response) {//if failed
                    $scope.data = response.data || "Request failed";
                    $scope.status = response.status;
                });
            };

            $scope.posttask = function (taskdata) {
                $scope.code = null;
                $scope.response = null;
                $http({method: "POST", url: $scope.url, data: taskdata, cache: $templateCache}).
                then(function (response) {  //if okay
                    $scope.status = response.status;
                    $scope.data = response.data;
                    console.log("posttask");
                    console.log(taskdata);

                }, function (response) { // if failed
                    $scope.data = response.data || "Request failed";
                    $scope.status = response.status;
                    console.log("error");
                });
            };

            $scope.postTaskData = function () {
                //"use strict";
                var taskdata = {
                    title: document.getElementById("task").value
                };
                $scope.posttask(taskdata);
            };



        }]);

