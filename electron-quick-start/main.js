'use strict';
const electron = require('electron');
const ipcMain = require('electron').ipcMain;
const app = electron.app;  // Module to control application life.
const BrowserWindow = electron.BrowserWindow;  // Module to create native browser window.
const globalShortcut = electron.globalShortcut;
var request = require('request');
/*const newWindow = app.setUserTasks([
 {
 program: "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe",
 arguments: '--new-window',
 iconPath: "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe",
 iconIndex: 0,
 title: 'New Window',
 description: 'Create a new window'
 }
 ]);*/
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let kazkas;


app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform != 'darwin') {
        app.quit();
    }
});

//This method will be called when Electron has finished
//initialization and is ready to create browser windows.
app.on('ready', function () {
    // Register a 'ctrl+x' shortcut listener.

    var ret = globalShortcut.register('ctrl+1', function () {
        console.log('ctrl+1 is pressed');
    });

    /* var win = new BrowserWindow({width: 800, height: 600, show: false});
     win.on('closed', function () {
     win = null;
     });*/

    /* win.loadURL('file://' + __dirname + '/test.html');*/

    /* win.loadURL('https://github.com');*/
    /*win.show();*/
    /*   mainWindow.loadURL('https://reddit.com');
     mainWindow.show();*/
    /*    win.loadURL('file://' + __dirname + '/test.html');*/


    mainWindow = new BrowserWindow({width: 800, height: 600});
    // and load the index.html of the app.
    mainWindow.loadURL('file://' + __dirname + '/index.html');
    // Open the DevTools.
    mainWindow.webContents.openDevTools();
    /*    mainWindow.webContents.loadURL('');*/

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

});

var counter = 0;
var startTime = 0;
var start = 0;
var end = 0;
var diff = 0;
var timerID = 0;
function chrono() {
    end = new Date();
    diff = end - start;
    diff = new Date(diff);
    var msec = diff.getMilliseconds();
    var sec = diff.getSeconds();
    var min = diff.getMinutes();
    var hr = diff.getHours() - 1;
    if (min < 10) {
        min = "0" + min
    }
    if (sec < 10) {
        sec = "0" + sec
    }
    if (msec < 10) {
        msec = "00" + msec
    }
    else if (msec < 100) {
        msec = "0" + msec
    }
    document.getElementById("chronotime").innerHTML = 0 + ":" + min + ":" + sec + ":" + msec;
    timerID = setTimeout("chrono()", 10)
}
function chronoStart() {
    document.chronoForm.startstop.value = "stop!";
    document.chronoForm.startstop.onclick = chronoStop;
    document.chronoForm.reset.onclick = chronoReset;
    start = new Date();
    chrono()
}
function chronoContinue() {
    document.chronoForm.startstop.value = "stop!";
    document.chronoForm.startstop.onclick = chronoStop;
    document.chronoForm.reset.onclick = chronoReset;
    start = new Date() - diff;
    start = new Date(start);
    chrono()
}
function chronoReset() {
    document.getElementById("chronotime").innerHTML = "0:00:00:000";
    start = new Date()
}
function chronoStopReset() {
    document.getElementById("chronotime").innerHTML = "0:00:00:000";
    document.chronoForm.startstop.onclick = chronoStart
}
function elements() {
    var aherfas = document.createElement("A");
    var btn = document.createTextNode(document.getElementById("project").value + " "
        + document.getElementById("chronotime").innerHTML);
    var li = document.createElement("P");
    li.appendChild(btn);
    aherfas.appendChild(li);
    document.body.appendChild(aherfas);
    $("p").click(function () {
        window.open('test.html');

        $("p").attr("id", "1");
    });
    $("p").hover(function () {
        $(this).css("background-color", "yellow");
    }, function () {
        $(this).css("background-color", "white");
    });

}


//function chronoStop() {
//    elements();
//    document.chronoForm.startstop.value = "start!";
//    document.chronoForm.startstop.onclick = chronoStart;
//    document.chronoForm.reset.onclick = new Date();
//
//    clearTimeout(timerID);
//
//}

