angular.module('httpExample', [])
    .controller('FetchController', ['$scope', '$http', '$templateCache',
        function ($scope, $http, $templateCache) {
            $scope.url = 'http://localhost/public/api/projects';
            $scope.taskurl = 'http://localhost/public/api/task';
            $scope.fetch = function () {
                $scope.code = null;
                $scope.response = null;
                $http({method: "GET", url: $scope.url, cache: $templateCache}).
                then(function (response) {//if okay
                    $scope.status = response.status;
                    $scope.data = response.data;
                    console.log($scope.data);
                }, function (response) {//if failed
                    $scope.data = response.data || "Request failed";
                    $scope.status = response.status;
                });
            };

            $scope.post = function (data) {
                $scope.code = null;
                $scope.response = null;
                $http({method: "POST", url: $scope.url, data: data, cache: $templateCache}).
                then(function (response) {  //if okay
                    $scope.status = response.status;
                    $scope.data = response.data;
                    console.log($scope.data);
                    console.log("data");
                }, function (response) { // if failed
                    $scope.data = response.data || "Request failed";
                    $scope.status = response.status;
                    console.log("error");
                });
            };

            $scope.posttask = function (taskdata) {
                $scope.code = null;
                $scope.response = null;
                $http({method: "POST", url: $scope.taskurl, data: taskdata, cache: $templateCache}).
                then(function (response) {  //if okay
                    $scope.status = response.status;
                    $scope.data = response.data;
                    console.log("posttask");
                    console.log(taskdata);

                }, function (response) { // if failed
                    $scope.data = response.data || "Request failed";
                    $scope.status = response.status;
                    console.log("error");
                });
            };

            $scope.postTaskData = function () {
                //"use strict";
                var taskdata = {
                    title: document.getElementById("task").value
                };
                $scope.posttask(taskdata);
            };

            $scope.showJson = function () {
                $scope.json = angular.fromJson(data);
            };

            $scope.updateModel = function (method, url) {
                $scope.method = method;
                $scope.url = url;
            };
            $scope.jsonlists = function (data) {
                angular.toJson(data);
            };

            $scope.chronoRunning = false;
            $scope.chronoToggle = function () {
                if ($scope.chronoRunning) {
                    // timer running, stop
                    elements();
                    document.chronoForm.startstop.value = "start!";
                    document.chronoForm.reset.onclick = new Date();
                    console.log("Pries POST");
                    var data = {
                        name: document.getElementById("project").value
                    };
                    clearTimeout(timerID);
                    $scope.chronoRunning = false;
                    $scope.post(data);
                    console.log(data);
                } else {
                    // timer stopped, activate
                    document.chronoForm.startstop.value = "stop!";
                    document.chronoForm.reset.onclick = chronoReset;
                    start = new Date();
                    chrono();
                    $scope.chronoRunning = true;
                    console.log("timer started");

                }
            };

        }]);

